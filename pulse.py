from subprocess import check_output

def robust_key_value_convert(lines):
    kv = {}
    for line in lines:
        key, value = line, None
        if ': ' in line:
            key, value = line.split(': ', maxsplit=1)
        elif ' = ' in line:
            key, value = line.split(' = ', maxsplit=1)

        kv[key.strip().lower()] = value.strip('\s"\'') if value else None

    return kv

def pacmd_to_kv(command):
    raw = check_output(['pacmd'] + command).decode('utf-8')
    output = raw.split('index:')

    infos = []
    for sink_raw in output[1:]:
        lines = [line.strip() for line in sink_raw.split('\n')]
        info = robust_key_value_convert(lines)
        info['index'] = lines[0]
        infos.append(info)

    return infos

if __name__ == '__main__':
    print('{:<5} {:<}'.format('Index', 'Description'))
    for sink in pacmd_to_kv(['list-sinks']):
        print('{:<5} {:<}'.format(sink['index'], sink['device.description']))

    print()
    print('{:<5} {:<60} {:<}'.format('Input', 'Index', 'Description'))
    for input in pacmd_to_kv(['list-sink-inputs']):
        print('{:<5} {:<60} {:<}'.format(input['index'], input['sink'], input['application.name']))
